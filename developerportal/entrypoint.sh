#!/bin/bash

sed -i "s/ENV_DB_PASSWORD/${ENV_DB_PASSWORD}/" /srv/http/developerportal.gnome.org/html/devportal/settings.py
sed -i "s/ENV_DB_HOST/${ENV_DB_HOST}/" /srv/http/developerportal.gnome.org/html/devportal/settings.py
sed -i "s/ENV_DJANGO_SECRET_KEY/${ENV_DJANGO_SECRET_KEY}/" /srv/http/developerportal.gnome.org/html/devportal/settings.py
sed -i "s/ALLOWED_HOSTS = \[\]/ALLOWED_HOSTS \= \[ 'developerportal.gnome.org' \]/" /srv/http/developerportal.gnome.org/html/devportal/settings.py

exec httpd -DFOREGROUND
